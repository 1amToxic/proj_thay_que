package com.example.BookStoreProj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.BookStoreProj.model.Payment;
import com.example.BookStoreProj.repository.PaymentRepository;

public interface PaymentDao {
	Payment addPayment(Payment p);
}
@Service
class PaymentDaoImpl implements PaymentDao{
	@Autowired
	PaymentRepository paymentRepo;
	@Override
	public Payment addPayment(Payment p) {
		Payment pAdd = this.paymentRepo.save(p);
		return pAdd;
	}
	
}