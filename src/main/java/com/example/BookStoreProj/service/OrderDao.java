package com.example.BookStoreProj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.BookStoreProj.model.Order;
import com.example.BookStoreProj.repository.OrderRepository;

public interface OrderDao {
	Order addOrder(Order o);
}
@Service
class OrderDaoImpl implements OrderDao{
	@Autowired
	OrderRepository orderRepo;
	@Override
	public Order addOrder(Order o) {
		Order oAdd = this.orderRepo.save(o);
		return oAdd;
	}
	
}