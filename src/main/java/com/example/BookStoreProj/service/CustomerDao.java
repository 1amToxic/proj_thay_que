package com.example.BookStoreProj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.BookStoreProj.model.Customer;
import com.example.BookStoreProj.repository.CustomerRepository;

public interface CustomerDao {
	boolean login(Customer c);
	boolean signup(Customer c);
	Customer getCustomerById(int id);
}
@Service
class CustomerDaoImpl implements CustomerDao{
	@Autowired
	private CustomerRepository customerRepo;

	@Override
	public boolean login(Customer c) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean signup(Customer c) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public Customer getCustomerById(int id) {
		return this.customerRepo.findById(id).orElse(null);
	}
	
}
