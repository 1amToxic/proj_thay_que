package com.example.BookStoreProj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.BookStoreProj.model.Cart;
import com.example.BookStoreProj.repository.CartRepository;

public interface CartDao {
	Cart addCart(Cart c);
}
@Service
class CartDaoImpl implements CartDao{
	@Autowired
	CartRepository cartRepo;
	@Override
	public Cart addCart(Cart c) {
		Cart cAdd = this.cartRepo.save(c);
		return cAdd;
	}
	
}