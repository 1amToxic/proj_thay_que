package com.example.BookStoreProj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookStoreProjApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookStoreProjApplication.class, args);
	}

}
